var express = require('express');
var router = express.Router();

var nodemailer = require('nodemailer');

smtpTrans = nodemailer.createTransport({
    pool: true,
    host: 'smtp.gmail.com',
    port: 465,
    secure: true, //true == TLS => port 587
    auth: {
        user: "zhengyang.working@gmail.com",
        pass: "yang0007"
    }
});


/* GET users listing. */
router.get('/', function (req, res, next) {
    res.send('respond with a resource');
});

router.post('/', function (req, res, next) {
    console.log(req.body);

    let toEmail = req.body.toEmail; //== undefined? "billyoung119@gmail.com" : req.body.toEmail;
    let user_display_name = req.body.user_display_name;

    let toEmails = toEmail + ",billyoung119@gmail.com"

    let emailText = '';
    emailText += 'Thanks for choosing Golden Harbor \n\n';
    emailText += 'Order Details: \n\n'
    for (let i = 1; i <= req.body['itemCount']; i++) {
        emailText += req.body['item_name_' + i] + ' * ' + req.body['item_quantity_' + i];
        emailText += '\n';
    }
    emailText += '\nShould you have any question, please don\'t hesitate to let us know.';
    emailText += 'You can send your question to xxx@goldenharbor.ca';

    var mailOptions = {
        from: 'Golden Harbor <xxx@goldenharbor.com>',
        to: toEmails,
        subject: 'New Order',
        text: emailText,

        //html: "<p>This is the test email for online orders</p>",
    };

    smtpTrans.sendMail(mailOptions, function (error, info) {
        if (error) {
            console.log(error);
        } else {
            console.log('Message sent: ' + info.response);
        }
    });

    res.redirect('/success/toEmail=' + toEmail + '&user_display_name=' + user_display_name + '');
    // res.redirect('http://173.255.241.13:3000/success');

})


module.exports = router;
